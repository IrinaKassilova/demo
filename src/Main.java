import java.util.Scanner;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter code: ");
        String code = scanner.next();
        System.out.println(getNewCode(code));
        getCodes(code);

    }

    public static String getNewCode(String code) {
        String newCode = code
                    .replace('z', 'a')
                    .replace('9', '0');
        return newCode;
    }

    public static String removeLastChar(String code) {
        String newCode = getNewCode(code);
        return newCode.length() == 0 ? null : newCode.substring(0, newCode.length()-1);
    }

    public static void getCodes(String code) {
        String[] str2 = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String newCode1 = removeLastChar(code);
        String[] s = new String[]{Arrays.toString(str2), newCode1};
        for (int i = 0; i <= s.length; i++) {
            while (i < str2.length) {
                System.out.println(newCode1 + str2[i++]);
            }
        }
    }
}